# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version
from frappe import _

app_name = "avium"
app_title = "Avium"
app_publisher = "Servipro"
app_description = "Gestion Competiciones"
app_icon = "octicon octicon-repo-clone"
app_color = "green"
app_email = "pau@servipro.eu"
app_license = "MIT"

portal_menu_items = [
	{"title": _("Registration"), "route": "/registration","reference_doctype": "Registration"},
]

integration_services = ["Avium Stickers"]

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/avium/css/avium.css"
# app_include_js = "/assets/avium/js/avium.js"

# include js, css files in header of web template
# web_include_css = "/assets/avium/css/avium.css"
# web_include_js = "/assets/avium/js/avium.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "avium.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "avium.install.before_install"
# after_install = "avium.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "avium.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"avium.tasks.all"
# 	],
# 	"daily": [
# 		"avium.tasks.daily"
# 	],
# 	"hourly": [
# 		"avium.tasks.hourly"
# 	],
# 	"weekly": [
# 		"avium.tasks.weekly"
# 	]
# 	"monthly": [
# 		"avium.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "avium.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "avium.event.get_events"
# }

