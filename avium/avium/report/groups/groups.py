# Copyright (c) 2013, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import get_doc

def execute(filters=None):
	columns = [
		"Class Code:Data:50",
		"Class Description:Data:250",
		"Class Ampliation:Data:250",
		"Cage:Link/Cage:150",
		"Ind Group:Data:70",
		"Team Group:Data:70",
		"Team Number:Data:50",
	]
	data = []

	docs = frappe.get_all("Group", filters=filters, fields=["class_description","class_code","class_ampliation","cage","team_group","ind_group","team_number"])
	for doc in docs:
		line = [
			doc.class_code,
			doc.class_description,
			doc.class_ampliation,
			doc.cage,
			doc.ind_group,
			doc.team_group,
			doc.team_number
		]
		data.append(line)

	return columns, data
