// Copyright (c) 2016, Servipro and contributors
// For license information, please see license.txt

function axis_values(arr) {
    var a = [], b = [], prev;
    
    arr.sort();
    for ( var i = 0; i < arr.length; i++ ) {
        if ( arr[i] !== prev ) {
            a.push(arr[i]);
            b.push(1);
        } else {
            b[b.length-1]++;
        }
        prev = arr[i];
    }
    
    return [a, b];
}

frappe.query_reports["Groups"] = {
	"filters": [
        {
            "fieldname":"family",
            "label": __("Family"),
            "fieldtype": "Link",
            "options": "Family",
            "default": ""
        }
    ],
    get_chart_data: function(columns, result) {
        result = axis_values($.map(result, function(d) { return d[0][0]; }))
        result[0].unshift("x"),
        result[1].unshift("Cantidad")

        return {
                data: {
                    x: 'x',
                    columns: [
                        result[0],
                        result[1]
                    ],
                },
                axis: {
                    x: {
                        type: 'category'
                    },
                    y: {
                        tick: {
                            format: function (d) {
                                return (parseInt(d) == d) ? d : null;
                            }
                        }
                    }
                },
                bar: {
                    width: {
                        ratio: 0.5
                    }
                },
                chart_type: 'bar',
            }
	}
}