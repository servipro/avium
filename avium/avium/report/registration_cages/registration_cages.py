# coding=utf-8
# Copyright (c) 2013, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns = [
		"DNI:Data:100",
		"Nombre Expositor:Data:250",
		"Grupo:Data:50",
		"Ind/Eq:Data:60",
		"Denominacion Clase:Data:250",
		"Jaula:Data:60",
		"Cria A:Data:50",
		"Año A:Data:50",
		"Ani A:Data:50",
		"Cria B:Data:50",
		"Año B:Data:50",
		"Ani B:Data:50",
		"Cria C:Data:50",
		"Año C:Data:50",
		"Ani C:Data:50",
		"Cria D:Data:50",
		"Año D:Data:50",
		"Ani D:Data:50",
	]
	data = []

	for registration in frappe.get_all("Registration",["name"]):
		doc = frappe.get_doc("Registration", registration.name)

		expositor = [
			doc.dni,
			doc.name
		]

		if doc.specimens:
			for reg in doc.specimens:
				specimen = [
					reg.group_code,
					"Ind",
					reg.group,
					reg.cage,
					reg.breeder_a,
					reg.ring_a,
					reg.year_a,
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					""
				]
				line = expositor+specimen
				data.append(line)

		if doc.specimens_team:
			for reg in doc.specimens_team:
				specimen = [
					reg.group_code,
					"Ind",
					reg.group,
					reg.cage,
					reg.breeder_a,
					reg.ring_a,
					reg.year_a,
					reg.breeder_b,
					reg.ring_b,
					reg.year_b,
					reg.breeder_c,
					reg.ring_c,
					reg.year_c,
					reg.breeder_d,
					reg.ring_d,
					reg.year_d,
				]
				line = expositor+specimen
				data.append(line)

	return columns, data
