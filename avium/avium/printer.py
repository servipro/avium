from __future__ import unicode_literals

import frappe
from frappe import _
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4

@frappe.whitelist()
def test_pdf():
    margeSuperior = 30
    margeInferior = 20
    margeEsquerra = 30
    margeDret = 20

    # **Configuracion tabla**

    numeroColumnes = 4
    ampladaColumnes = 130
    alcadaFiles = 90
    marges = True
    separacioLinies = 15
    tamanyText = 9

    data = {
        'BLANCO RECESIVO': {
            'D0001': ('A-397','C-397','B-397','D-397','D-297','A-297','C-297','B-297','A-197','B-197','C-197','D-197'),
            'D0002': ('397','207','1973','295','297','257','797','197','097','390','067'),
        },
        'AMARILLO INTENSO':{
            'D0005': ('A-397','C-397','B-397','A-297','D-397','D-297','C-297','B-297','A-197','B-197','C-197','D-197') ,
            'D0006': ('397','207','1973','295','297','257','797','197','097','390','067') ,
        },
    }

    amplada, alcada = A4

    # Inicialitzem variables

    pdf = canvas.Canvas('test2.pdf')

    columnaActual = 0
    posicioColumna = margeEsquerra + columnaActual * ampladaColumnes + (
                                                                       amplada - margeEsquerra - margeDret - numeroColumnes * ampladaColumnes) / 2

    alcadaActual = alcada - margeSuperior

    pdf.setFont('Times-Roman', tamanyText)

    # Iniciem loop

    for seccio in data.keys():
        for competicio in data[seccio].keys():

            # Ordenem gabies per numero i lletra

            jaulesRaw = sorted(data[seccio][competicio])

            numJaules = []
            jaules = []

            for numJaula in jaulesRaw:
                numJaules.append(numJaula[-3:])

            i = 0

            for preJaula in sorted(numJaules):
                while jaulesRaw[i][-3:] != preJaula:
                    i = i + 1
                jaules.append(jaulesRaw.pop(i))
                i = 0

            # Loop per dibuixar la taula

            for jaula in jaules:

                # Augmentem una posicio en la mateixa columna

                alcadaActual = alcadaActual - alcadaFiles

                # Si hem arribat al final de la columna, canviem de columna

                if alcadaActual < margeInferior:
                    columnaActual = columnaActual + 1

                    # Si hem arribat al numero de columnes configurades, canviem de fulla

                    if columnaActual >= numeroColumnes:
                        pdf.showPage()
                        pdf.setFont('Times-Roman', tamanyText)
                        columnaActual = 0
                    alcadaActual = alcada - margeSuperior - alcadaFiles
                    posicioColumna = margeEsquerra + columnaActual * ampladaColumnes + (
                                                                                       amplada - margeEsquerra - margeDret - numeroColumnes * ampladaColumnes) / 2

                # Dibuixem marge si es configura

                if marges:
                    pdf.rect(posicioColumna, alcadaActual, ampladaColumnes, alcadaFiles, fill=0)

                # Dibuixem el text

                pdf.drawString(posicioColumna + 5, alcadaActual + alcadaFiles / 2 + separacioLinies, 'GRUPO:')
                pdf.drawCentredString(posicioColumna + ampladaColumnes / 2, alcadaActual + alcadaFiles / 2, seccio)
                pdf.drawString(posicioColumna + 5, alcadaActual + alcadaFiles / 2 - 1.5 * separacioLinies, 'N JAULA:')

                pdf.saveState()
                pdf.setFont('Times-Bold', tamanyText)
                pdf.drawCentredString(posicioColumna + ampladaColumnes / 2,
                                      alcadaActual + alcadaFiles / 2 + separacioLinies, competicio)
                pdf.drawCentredString(posicioColumna + ampladaColumnes / 2,
                                      alcadaActual + alcadaFiles / 2 - 1.5 * separacioLinies, jaula)
                pdf.restoreState()

    pdf.showPage()

    pdf.save()