# -*- coding: utf-8 -*-
# Copyright (c) 2015, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import sqlite3
from frappe import _

class Database(Document):
	def before_submit(self):
		if self.file and len(self.file)>0 and self.path and len(self.path)>0:
			print "good"
		else:
			frappe.throw(_("File and path must have a value"))

	def on_cancel(self):
		frappe.delete_doc("Class", [c.name for c in frappe.get_all("Class",
			filters={
				"database": self.name
			}
		)], ignore_permissions=True)

	def on_submit(self):
		if self.file and self.path:
			conn = sqlite3.connect(self.path+self.file)
			c = conn.cursor()
			for clase in c.execute("SELECT codigo, familia_id, desc FROM tablas_clase"):
				doc = frappe.new_doc("Class")
				family = frappe.get_list("Family", filters={"code":clase[1]})[0]

				doc.update({
					"database":self.name,
					"family":family.name,
					"description":clase[2],
					"code":clase[0],
					"international":clase[0].endswith("I")
				})

				doc.insert()
