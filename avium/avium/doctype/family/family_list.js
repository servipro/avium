/**
 * Created by pau on 16/09/16.
 */
frappe.listview_settings['Family'] = {
	onload: function(listview) {

        listview.page.add_action_item(__("Generate Base Families"), function() {
            frappe.call({
                method:"avium.avium.utils.generate_families",
                callback:function(r){
                    console.log(r.message);
                }
            });
		});

	}
};