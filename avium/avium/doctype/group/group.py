# -*- coding: utf-8 -*-
# Copyright (c) 2015, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from avium.avium import utils

class Group(Document):
	def autoname(self):
		self.name = self.class_description + " " + (self.class_ampliation or "")

	def before_save(self):
		championship = utils.get_championship()
		if self.team_group or self.ind_group:
			return
		else:
			family=self.class_code[0]			
			groups=frappe.db.sql("""select class_code from `tabGroup` where class_code like %s """, family+"%")
			next_group=1+len(groups)*2
			found=False
			while not found:
				str_group = str(next_group).zfill(4)
				exists = len(frappe.db.sql("""select ind_group,team_group from `tabGroup` where ind_group = %s OR team_group = %s""", [family+str_group, family+str_group]))>0
				if not exists:
					if championship.groups_pair_numbers==0:
						self.ind_group=family+str(next_group).zfill(4)
						self.team_group=family+str(next_group+1).zfill(4)
					else:
						self.team_group=family+str(next_group).zfill(4)
						self.ind_group=family+str(next_group+1).zfill(4)
					found=True
				else:
					next_group+=2
			
			
		
