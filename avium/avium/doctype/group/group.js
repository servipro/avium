// Copyright (c) 2016, Servipro and contributors
// For license information, please see license.txt

var set_query_groups = function(frm,database,international){
	frm.set_query("class", function(doc, cdt, cdn) {
			return {
				filters: {
					database: database,
					international: international
				}
			};
	});
}

frappe.ui.form.on('Group', {
	onload: function(frm) {
		frappe.call({
			"method": "avium.avium.utils.get_championship",
			callback: function (data) {
				frm.set_query("class", function(doc, cdt, cdn) {
						return {
							filters: {
								database: data.message.database,
								international: data.message.international
							}
						};
				});
			}
		})
    }
})