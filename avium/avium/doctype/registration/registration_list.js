frappe.listview_settings['Registration'] = {
	onload: function(listview) {

        listview.page.add_action_item(__("Assign Cage Number"), function() {
            frappe.call({
                method:"avium.avium.utils.assign_cage_number",
                callback:function(r){
                    console.log(r.message);
                }
            });
		});

	}
};