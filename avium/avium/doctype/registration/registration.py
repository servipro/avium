# -*- coding: utf-8 -*-
# Copyright (c) 2015, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Registration(Document):
	def before_save(self):
		for item in self.specimens:
			group = frappe.get_doc("Group", item.group)
			item.group_code = group.ind_group
		for item in self.specimens_team:
			group = frappe.get_doc("Group", item.group)
			item.group_code = group.team_group