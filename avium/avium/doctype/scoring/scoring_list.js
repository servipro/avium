/**
 * Created by pau on 16/09/16.
 */
frappe.listview_settings['Scoring'] = {
    colwidths: {"subject": 1},
    hide_name_column: true,
	onload: function(listview) {

        listview.page.add_action_item(__("Generar Grupos"), function() {
                frappe.call({
                    method:"avium.avium.utils.generate_scoring_groups",
                    callback:function(r){
                        console.log(r.message);
                    }
                });
	    });

        listview.page.add_action_item(__("Rellenar Grupos"), function() {
                frappe.call({
                    method:"avium.avium.utils.populate_scoring_groups",
                    callback:function(r){
                        console.log(r.message);
                    }
                });

    	});

        listview.page.add_action_item(__("Assign Judges to Groups"), function() {

            var dialog = new frappe.ui.Dialog({
                title: __('Number of specimens per judge'),
                fields: [
                    {fieldtype: "Section Break"},
                    {"fieldtype": "Int" , "fieldname": "number" , "label": "Specimens per judge",},
                    {fieldname: 'ok_button', fieldtype:'Button', label:'Accept'}
                ]
            });

            dialog.fields_dict.ok_button.input.onclick = function() {
                var v = listview.dialog.get_values();

                frappe.call({
                    method:"avium.avium.utils.assign_judges_groups",
                    callback:function(r){
                        console.log(r.message);
                        listview.dialog.hide();
                    }
                });
            };

            dialog.show();

            listview.dialog = dialog;
		});

	}
};

