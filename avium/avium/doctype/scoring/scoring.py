# -*- coding: utf-8 -*-
# Copyright (c) 2015, Servipro and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Scoring(Document):
	def autoname(self):
		self.name = self.group_code