from __future__ import unicode_literals

import frappe
from frappe import _

@frappe.whitelist()
def get_championship():
    return frappe.db.get("Championship")


@frappe.whitelist()
def assign_cage_number():
    frappe.db.sql("select @rank:=0")
    frappe.db.sql("""update `tabRegistration Specimens` set cage = (select @rank:= @rank + 1)""")
    frappe.db.sql("""update `tabRegistration Specimen Team` set cage = (select @rank:= @rank + 1)""")

    frappe.msgprint(_("Cage numbers assigned"))

@frappe.whitelist()
def generate_families():
    fam = frappe.new_doc("Family")
    fam.code = "A"
    fam.family_name = "A"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "B"
    fam.family_name = "B"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "C"
    fam.family_name = "C"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "D"
    fam.family_name = "D"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "E"
    fam.family_name = "E"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "F"
    fam.family_name = "F"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "G"
    fam.family_name = "G"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "H"
    fam.family_name = "H"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "I"
    fam.family_name = "I"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "J"
    fam.family_name = "J"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "K"
    fam.family_name = "K"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "L"
    fam.family_name = "L"
    fam.save()
    fam = frappe.new_doc("Family")
    fam.code = "M"
    fam.family_name = "M"
    fam.save()
    frappe.msgprint(_("Families Generated"))

import string, random
@frappe.whitelist()
def test_perf():
    groups = frappe.get_all("Group", fields=["name"])
    for i in range(0,5000):
        contact = frappe.get_doc({
            "doctype":"TestPDC",
            "nom": ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(12)),
            "cognom": "apellido apellido2 apellido3",
            "adreca":"Carrer saragossa 90",
            "poblacio": "Amposta",
            "codi_postal": random.randrange(10000, 50000),
            "telefon": "977702779",
            "mobil": "617489409",
            "correu": "paurosello@gmail.com",
            "facebook": "nom de facebook",
            "sexe": random.randrange(1, 10)%2==0,
            "dni": "",
            "data_naixement":"2013-02-23",
            "profesio":"Enginer mecanic total",
            "estudis": "Carrera de informatica per la UPM",
            "signatura": "SI",
            "group": groups[random.randrange(0, len(groups)-1)].name,
        })

        for j in range(0, 5):
            contact.append("test1", {"ambit": "ambit1", "tema":"tema1"})

        for k in range(0, 5):
            contact.append("test2", {"ambit": "ambit2", "tema":"tema2"})

        contact.insert()

    frappe.msgprint(_("Finish"))


@frappe.whitelist()
def generate_scoring_groups():
    for group in frappe.get_all("Group", fields=["name", "ind_group", "team_group"]):
        scoring_ind = frappe.get_doc({"doctype":"Scoring", "group": group.name, "group_code":group.ind_group , "is_team":0})
        scoring_ind.insert()

        scoring_eq = frappe.get_doc({"doctype":"Scoring", "group": group.name, "group_code":group.team_group , "is_team":1})
        scoring_eq.insert()

    frappe.msgprint(_("Scoring Groups Generated"))

@frappe.whitelist()
def populate_scoring_groups():
    assign_cage_number()
    for group in frappe.get_all("Group", fields=["name", "ind_group", "team_group"]):
        scoring_ind = frappe.get_doc("Scoring", group.ind_group)
        for reg in frappe.get_all("Registration Specimens", filters={"group_code": group.ind_group}, fields=["cage"]):
            scoring_ind.append("scoring",{ "cage": reg.cage })
        scoring_ind.save()

        scoring_eq = frappe.get_doc("Scoring",group.team_group)
        for reg in frappe.get_all("Registration Specimen Team", filters={"group_code": group.team_group}, fields=["cage"]):
            scoring_eq.append("scoring",{ "cage": reg.cage })
        scoring_eq.save()

    frappe.msgprint(_("Scoring Groups Populated"))

@frappe.whitelist()
def assign_judges_groups():
    frappe.msgprint(_("Cage numbers assigned"))

def update_my_account_context(context):
    context["sidebar_items"].extend([
		{"label": _("Registration"), "url": "registration"},    	
		{"label": _("Specimens"), "url": "specimen-registration"}
	])

if __name__ == '__main__':
    print "ola"