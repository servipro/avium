groups = [
    {"group":"D0002","n":12},
    {"group":"C9234","n":25},
    {"group":"I0022","n":5},
    {"group":"C0123","n":5},
    {"group":"D0231","n":5},
    {"group":"I0123","n":2},
    {"group":"D0042","n":2},
    {"group":"C0034","n":25},
    {"group":"I0052","n":15},
    {"group":"C0193","n":5},
    {"group":"D0230","n":5},
    {"group":"I0923","n":2}
]

judges = [
    {"id":1, "clases":["I"]},
    {"id":1, "clases": ["I","C"]},
    {"id":1, "clases": ["C"]},
    {"id":1, "clases": ["D"]},
    {"id":1, "clases": ["D", "C"]}
]

ngroups = sorted(groups, key=lambda x: x["n"], reverse=True)

print ngroups

