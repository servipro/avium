# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Avium",
			"color": "green",
			"icon": "octicon octicon-repo-clone",
			"type": "module",
			"label": _("Avium")
		}
	]
