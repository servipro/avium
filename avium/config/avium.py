from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Configuration"),
			"items": [
                {
                    "type": "doctype",
                    "name": "Championship",
                    "label": _("Championship"),
                },
				{
					"type": "doctype",
					"name": "Federation",
                    "label": _("Federations"),
				},
				{
					"type": "doctype",
					"name": "Association",
                    "label": _("Associations"),
				},
				{
					"type": "doctype",
					"name": "Expositor",
                    "label": _("Expositors"),
				},
				{
					"type": "doctype",
					"name": "Judge Association",
                    "label": _("Judge Associations"),
				},
				{
					"type": "doctype",
					"name": "Judge",
                    "label": _("Judges"),
				},
				{
					"type": "doctype",
					"name": "Family",
                    "label": _("Families"),
				},
				{
					"type": "doctype",
					"name": "DB",
                    "label": _("DBs"),
				},
                {
                    "type": "page",
                    "label": _("Dashboard"),
                    "name": "Dashboard",
                }
			]
		},
        {
            "label": _("Before Start"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Groups"),
                    "name": "Group"
                },
                {
                    "type": "doctype",
                    "label": _("Registrations"),
                    "name": "Registration"
                }
            ]
        },
        {
            "label": _("Results"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Scorings"),
                    "name": "Scoring"
                }
            ]
        },
        {
            "label": _("Reports"),
            "items": [
                {
                    "type": "report",
                    "doctype": "Group",
                    "is_query_report": True,
                    "name": "Groups"
                },
                {
                    "type": "report",
                    "doctype": "Registration",
                    "is_query_report": True,
                    "name": "Registration Cages"
                }
            ]
        }
    ]