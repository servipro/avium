
from os import path, sep
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import re

# **Funcio per importar les fonts**

def importarFonts():

    directoriFonts = path.dirname(path.abspath(__file__)) + sep + 'Fonts'

    pdfmetrics.registerFont(TTFont('FreeSerif', path.join(directoriFonts, 'FreeSerif.ttf')))
    pdfmetrics.registerFont(TTFont('FreeSerifBold', path.join(directoriFonts, 'FreeSerifBold.ttf')))
    pdfmetrics.registerFont(TTFont('FreeSerifItalic', path.join(directoriFonts, 'FreeSerifItalic.ttf')))
    pdfmetrics.registerFont(TTFont('FreeSerifBoldItalic', path.join(directoriFonts, 'FreeSerifBoldItalic.ttf')))

# **Funcio per ordenar una llista o tupla per lletra i numero**

def ordenarLletraNumero(data):

    dataSplit = []
    dataSorted = []

    for i in data:
        dataSplit.append((re.findall('[^0-9]+', i), re.findall('[0-9]+', i)))

    dataSplitSortedNumero = sorted(dataSplit, key=lambda xifra: int(xifra[1][0]))

    dataSplitSortedLletraNumero = sorted(dataSplitSortedNumero, key=lambda lletra: lletra[0])

    for i in dataSplitSortedLletraNumero:
        dataSorted.append(''.join(i[0] + i[1]))

    return dataSorted

# **Funcio per ordenar una llista o tupla per numero i lletra**

def ordenarNumeroLletra(data):

    dataSplit = []
    dataSorted = []

    for i in data:
        dataSplit.append((re.findall('[^0-9]+', i), re.findall('[0-9]+', i)))

    dataSplitSortedLletra = sorted(dataSplit, key=lambda lletra: lletra[0])

    dataSplitSortedNumeroLletra = sorted(dataSplitSortedLletra, key=lambda xifra: int(xifra[1][0]))

    for i in dataSplitSortedNumeroLletra:
        dataSorted.append(''.join(i[0] + i[1]))

    return dataSorted