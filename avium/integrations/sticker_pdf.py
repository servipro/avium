import os
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from commonFunctions import importarFonts, ordenarNumeroLletra, ordenarLletraNumero
import frappe


def genPDF(parameters, settings):

    # **Configuracio pagina** - En centimetres
    margeSuperior = float(settings.upperMargin)
    margeInferior = float(settings.lowerMargin)
    margeEsquerra = float(settings.leftMargin)
    margeDret = float(settings.rightMargin)

    # **Configuracio tabla**
    numeroColumnes = int(settings.columns)
    numeroFiles = int(settings.rows)
    marges = float(settings.margin)
    separacioLinies = float(settings.lineSeparation)
    tamanyText = int(settings.textSize)

    # En centimetres
    ampladaColumnes = float(settings.columnWidth)
    alcadaFiles = float(settings.rowHeight)

    # Inici impressio
    # TODO fan falta agregar aquests dos parametres per configurar per quina etiqueta es comenca a imprimir

    columnaInicial = 2
    filaInicial = 4

    data = {}
    groups = frappe.get_all("Group", fields=["name", "ind_group", "team_group"])
    for group in groups:
        data[group.name]={}
        data[group.name][group.ind_group] = [int(x.cage) for x in frappe.get_all("Registration Specimens", fields=["cage"], filters={"group_code":group.ind_group})]
        data[group.name][group.team_group] = [int(x.cage) for x in frappe.get_all("Registration Specimen Team", fields=["cage"], filters={"group_code":group.team_group})]

    # **Importem les fonts**

    importarFonts()

    # **Guardem la alcada i la amplada total de la pagina**

    amplada, alcada = A4

    # **Inicialitzem variables**

    pdf_name = os.path.join("/tmp/"+"avium-pdf-{0}.pdf".format(frappe.generate_hash()))
    pdf = canvas.Canvas(pdf_name)

    columnaActual = columnaInicial - 1

    filaActual = filaInicial - 1

    ampladaColumnesImpresa = min((ampladaColumnes * cm, (amplada - margeEsquerra * cm - margeDret * cm) / numeroColumnes))

    posicioColumna = margeEsquerra * cm + columnaActual * (ampladaColumnesImpresa + (amplada - margeEsquerra * cm - margeDret * cm - ampladaColumnesImpresa * numeroColumnes) / (numeroColumnes - 1))

    alcadaActual = alcada - margeSuperior * cm - filaActual * alcadaFiles * cm + (1 - filaActual) * (alcada - margeSuperior * cm - margeInferior * cm - numeroFiles * alcadaFiles * cm) / (numeroFiles - 1)

    pdf.setFont('FreeSerif', tamanyText)

    # **Ordenem seccio i competicio per lletra i numero** - Funcio del arxiu commonFunctions.py

    competicioSeccio = dict()

    for seccio in data.keys():

        for competicio in data[seccio].keys():

            competicioSeccio[competicio] = seccio

    competicionsSorted = ordenarLletraNumero(competicioSeccio.keys())

    # **Iniciem loop**

    for competicio in competicionsSorted:

        seccio = competicioSeccio[competicio]

        # **Ordenem gabies per numero i lletra** - Funcio del arxiu commonFunctions.py

        jaulesSorted = ordenarNumeroLletra(data[seccio][competicio])

        # **Loop per dibuixar la taula**

        for jaula in jaulesSorted:

            # **Augmentem una posicio en la mateixa columna**

            alcadaActual = alcadaActual - alcadaFiles * cm - (alcada - margeSuperior * cm - margeInferior * cm - numeroFiles * alcadaFiles * cm) / (numeroFiles - 1)

            # **Si hem arribat al final de la columna, afegim una columna**

            if filaActual >= numeroFiles:
                columnaActual = columnaActual + 1

                filaActual = 1

                # **Si hem arribat al numero de columnes configurades, canviem de fulla**

                if columnaActual >= numeroColumnes:
                    pdf.showPage()
                    pdf.setFont('FreeSerif', tamanyText)
                    columnaActual = 0
                alcadaActual = alcada - margeSuperior * cm - alcadaFiles * cm
                posicioColumna = margeEsquerra * cm + columnaActual * (ampladaColumnesImpresa + (amplada - margeEsquerra * cm - margeDret * cm - ampladaColumnesImpresa * numeroColumnes) / (numeroColumnes - 1))

            # **Si no hem arribat al final de la columna, afegim una fila**

            else:
                filaActual += 1

            # **Dibuixem marge si es configura**

            if marges:
                pdf.rect(posicioColumna, alcadaActual, ampladaColumnesImpresa, alcadaFiles * cm, fill=0)

            # **Dibuixem el text**

            pdf.drawString(posicioColumna + 5, alcadaActual + alcadaFiles * cm / 2 + 1.5 * separacioLinies - tamanyText / 2, 'GRUPO:')
            pdf.drawString(posicioColumna + 5, alcadaActual + alcadaFiles * cm / 2 - 1.5 * separacioLinies - tamanyText / 2, 'N JAULA:')

            seccioSeparat = seccio.split()

            if len(seccio) * tamanyText * 0.8 < ampladaColumnes * cm:

                pdf.drawCentredString(posicioColumna + ampladaColumnesImpresa / 2, alcadaActual + alcadaFiles * cm / 2 - tamanyText / 2, seccio)

            else:

                contadorParaules = 1
                seccioLinia1 = seccioSeparat[0]
                seccioLinia2 = ''

                while contadorParaules < len(seccioSeparat) - 1:

                    if (len(seccioLinia1) + 1 + len(seccioSeparat[contadorParaules])) * 0.8 < ampladaColumnes * cm:
                        seccioLinia1 += ' ' + seccioSeparat[contadorParaules]
                    else:
                        seccioLinia2 += seccioSeparat[contadorParaules] + ' '

                    contadorParaules += 1

                seccioLinia2 += seccioSeparat[-1]

                pdf.drawCentredString(posicioColumna + ampladaColumnesImpresa / 2, alcadaActual + alcadaFiles * cm / 2 + 0.5 * separacioLinies - tamanyText / 2, seccioLinia1)
                pdf.drawCentredString(posicioColumna + ampladaColumnesImpresa / 2, alcadaActual + alcadaFiles * cm / 2 - 0.5 * separacioLinies - tamanyText / 2, seccioLinia2)

        pdf.saveState()
        pdf.setFont('FreeSerifBold', tamanyText)
        pdf.drawCentredString(posicioColumna + ampladaColumnesImpresa / 2, alcadaActual + alcadaFiles * cm / 2 + 1.5 * separacioLinies - tamanyText / 2, competicio)
        pdf.drawCentredString(posicioColumna + ampladaColumnesImpresa / 2, alcadaActual + alcadaFiles * cm / 2 - 1.5 * separacioLinies - tamanyText / 2, jaula)
        pdf.restoreState()

    pdf.showPage()

    pdf.save()

    return pdf_name