from __future__ import unicode_literals
import frappe
from frappe import _
import urllib, json
from frappe.utils import get_url, call_hook_method
from frappe.integration_broker.integration_controller import IntegrationController
from sticker_pdf import genPDF

class Controller(IntegrationController):
    service_name = 'Avium Stickers'
    parameters_template = [
        ##Margins
        {
            "label": "Upper Margin",
            'fieldname': 'upperMargin',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 1
        },
        {
            "label": "Lower Margin",
            'fieldname': 'lowerMargin',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 1
        },
        {
            "label": "Left Margin",
            'fieldname': 'leftMargin',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 1
        },
        {
            "label": "Right Margin",
            'fieldname': 'rightMargin',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 1
        },
        ##Table
        {
            "label": "Columns",
            'fieldname': 'columns',
            'fieldtype': "Int",
            'reqd': 1,
            'default': 4
        },
        {
            "label": "Rows",
            'fieldname': 'rows',
            'fieldtype': "Int",
            'reqd': 1,
            'default': 10
        },
        {
            "label": "Margin",
            'fieldname': 'margin',
            'fieldtype': "Check",
            'reqd': 1,
            'default': True
        },
        {
            "label": "Line Separation",
            'fieldname': 'lineSeparation',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 15
        },
        {
            "label": "Text Size",
            'fieldname': 'textSize',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 9
        },
        ##
        {
            "label": "Column Width",
            'fieldname': 'columnWidth',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 4.1
        },
        {
            "label": "Row Height",
            'fieldname': 'rowHeight',
            'fieldtype': "Data",
            'reqd': 1,
            'default': 2.5
        }

    ]

    # do also changes in razorpay.js scheduler job helper
    scheduled_jobs = [
        {
            "all": [
                # "frappe.integrations.razorpay.capture_payment"
            ]
        }
    ]

    # js = "assets/integrations/js/avium_stickers.js"

    # supported_currencies = ["INR"]

    def get_settings(self):
        if hasattr(self, "parameters"):
            return frappe._dict(self.parameters)

        custom_settings_json = frappe.db.get_value("Integration Service", "Avium Stickers",
                                                   "custom_settings_json")

        if custom_settings_json:
            return frappe._dict(json.loads(custom_settings_json))

    def enable(self, parameters, use_test_account=0):
        self.parameters = parameters
        self.use_test_account = use_test_account
        pass





@frappe.whitelist()
def print_pdf_stickers():
    doc = frappe.get_doc("Integration Service", "Avium Stickers")
    Controller.parameters = json.loads(doc.custom_settings_json)

    parameters = Controller().get_parameters()
    settings = Controller().get_settings()

    pdf_path = genPDF(parameters, settings)
    with open(pdf_path, "rb") as fileobj:
        filedata = fileobj.read()


    frappe.local.response.filename = "Stickers.pdf"
    frappe.local.response.filecontent = filedata
    frappe.local.response.type = "download"

    return

